#!/bin/bash

echo "===BUILD==="
npm install --save-dev gulp-zip
node_modules/.bin/gulp
echo "===CREATE BUCKET AND UPLOAD==="
aws s3api create-bucket --bucket lagartixa-lambda-functions --region eu-west-1 --create-bucket-configuration LocationConstraint=eu-west-1
aws s3 cp target/nodelambda.zip s3://lagartixa-lambda-functions
echo "===CLEANUP OLD STACK==="
aws cloudformation delete-stack --stack-name stk1
aws cloudformation wait stack-delete-complete --stack-name stk1
echo "===DEPLOY NEW STACK==="
aws cloudformation deploy --template-file template.yaml --stack-name stk1 --capabilities CAPABILITY_IAM
aws cloudformation wait stack-create-complete --stack-name stk1
echo "===CERATED EDGE API URL==="
aws cloudformation describe-stacks --stack-name stk1 \
    --query 'Stacks[0].Outputs[?OutputKey==`InputEndpoint`].OutputValue' \
    --output text