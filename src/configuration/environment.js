const environment = process.env.environment || 'dev';
const config = require(`./${environment}.json`);
const dataSourceEndpoint = process.env.DATA_ENDPOINT;
const dataSourceStage = process.env.DATA_SOURCE_STAGE;


module.exports = {
    scope: config.scope,
    failScope: config.failScope,
    status: config.status,
    dataSourceEndpoint,
    dataSourceStage,
    dataSourceResource: config.dataSourceResource
}