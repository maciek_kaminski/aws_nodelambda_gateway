const https = require('https');
const querystring = require('querystring');
const env = require('./configuration/environment');
const workflow = require('./services/workflow-service');
const util = require('./services/util-service');

exports.handler = (event, context, callback) => {
  // As this is not an ASYNC function lambda will wait for the empty event-loop.
  // Uncomment this and You will not see the logs from the http call. It will not wait for it.
  // Tip: context.callbackWaitsForEmptyEventLoop = false;

    console.log("INPUT EVENT: " + JSON.stringify(event));
    if (event.body !== null && event.body !== undefined) {
      var body = JSON.parse(event.body);
    } else {
      body = event;
    }

    var queryParams = {
      "status": env.status,
      "scope": env.scope
    };
    var failQueryParams = {
      "status": env.status,
      "scope": env.failScope
    };
    var headers = {
      "Content-Type":"application/json"
    };

    var path = ["/", env.dataSourceStage, "/", env.dataSourceResource].join("");
    var failPath = [path, '?', querystring.stringify(failQueryParams)].join("");
    var params = {
      host: env.dataSourceEndpoint,
      path: [path, '?', querystring.stringify(queryParams)].join(""),
      method: 'GET',
      headers: headers
    };
    console.log("PARAMS FOR THE REQUEST: "+ JSON.stringify(params));
    body.failPath = failPath;
    workflow.invoke(body, params).then(result => {
        console.log('AFTER WORKFLOW INVOKE: ' + body.service);
        callback(null, util.generateHttpResponse(result.statusCode, result.body, true)); //true to satisfy the proxy response format
    }).catch(err => {
        console.error("ERROR FROM INVOKED: " + JSON.stringify(err))
        callback(null, util.generateHttpResponse(err.statusCode, err.body, true)); //true to satisfy the proxy response format
    });
};
