const https = require('https');
const util = require('./util-service');

function invoke(request, params){
    console.log("START WORKFLOW HANDLING: " + request.service)
    switch(request.service){
        case 'workflow.okCall':
            return okCall(request.data, params);
        case 'workflow.failCall':
            return failCall(request.data, params, request.failPath);
        default:
            return Promise.reject(
                util.generateHttpResponse(405, "Workflow: " + request.service + " not found")
            );
    }
}

function okCall(event, params){
    return new Promise(function(resolve, reject) {
        makeAndHandleCall(resolve, reject, params);
    });
}

function failCall(event, params, failPath){
    params.path = failPath;
    console.log("FAIL PATH is invoked with: " + params.path);
    return new Promise(function(resolve, reject) {
        makeAndHandleCall(resolve, reject, params);
    });
}

function makeAndHandleCall(resolve, reject, params){
    var req = https.request(params, function(res) {
        let data = '';
        console.log('HTTP RESPONSE STATUS: ' + res.statusCode);
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            data += chunk;
        });
        res.on('end', function() {
            console.log("HTTP RESPONSE DONE: ");
            console.log(util.generateHttpResponse(res.statusCode, JSON.parse(data)));
            if(Math.floor(res.statusCode / 100) == 4 ||  Math.floor(res.statusCode / 100) == 5){
                reject(util.generateHttpResponse(res.statusCode, JSON.parse(data)));
            } else {
                resolve(util.generateHttpResponse(res.statusCode, JSON.parse(data)));
            }
        });
        res.on('error', function(err) {
            console.log("HTTP CALL -ERROR- " + err);
            reject(
                util.generateHttpResponse(res.statusCode, JSON.parse(err))
            );
        });
    });
    req.end();
}

module.exports = {
    invoke
}