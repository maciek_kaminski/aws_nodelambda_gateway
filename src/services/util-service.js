function generateHttpResponse(statusCode, body, isStringify){
    return {
            statusCode: statusCode,
            body: isStringify == null || !isStringify  ? body : JSON.stringify(body)
        }
}

module.exports = {
    generateHttpResponse
}