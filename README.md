# AWS NodeLambda Gateways CloudFormation
###Architecture
 - AWS API Gateway **'ApiGatewayEdgeRestApi'** which waits for an external call and will forward it to the Lambda. 
 Input Api Gateway has just 1 POST method - **'/execLambda'**
 - AWS API Gateway **'ApiGatewayMockDatasourceApi'** working as a mocked data-source. 
 Lambda calls it via HTTPS (to its stage via env variable) and fetches the result data.
 - Lambda written in Node. It is written alike tha Node Lambda from HSBC HK. 
 The main workflow is to make n HTTPS call to some url provided in the env variable - in this situation its the Mock API address.

###Deploy
The best way is to use the ***script***:
* **deploy.sh**

It does a lot of things:
* builds the project
* creates (if necessary) an AWS S3 bucket
* copies the zip file to this S3 bucket
* deletes the already existing stack with the same name
* creates a new stack using *template.yaml*
* returns the url of Edge API Gateway 


###Input data and execution
The input data to the whole stack can be one of 3 JSON objects provided in files located in */documetnation* directory:
* ok.json
* fail.json
*  noService.json
Just use the content of those files as data in a POST call to Edge API Gateway (with application/json header).

You can use a ***script***:
 * **run.sh**
It uses AWS CLI to obtain url of the current Edge API and makes a CURL POST call.
It takes one parameter which can have one of 3 values:
* ok
* fail
* noService
 
 
 ### What does the Lambda do
 The main task of this lambda is to make an HTTPS call to a provided url. 
 This url is our Mock API Gateway. There are 3 main workflows:
 * positive flow with 200 response code
 * negative answer from the Mock Api with 501 status code
 * negative 405 answer because user selected a not existing workflow
 
To understand it more take a look at the **Sequence diagram**.  
Input data for all of those scenarios in discussed in section above.

###CloudFormation prepare with Gulp
```
npm install --save-dev gulp-zip
node_modules/.bin/gulp
#create an S3...
aws s3api create-bucket --bucket lagartixa-lambda-functions --region eu-west-1 --create-bucket-configuration LocationConstraint=eu-west-1
aws s3 cp target/nodelambda.zip s3://lagartixa-lambda-functions
aws cloudformation deploy --template-file template.yaml --stack-name stk1 --capabilities CAPABILITY_IAM

#or package the lambda code to S3 with package!
#No Zipping, no sending
#The problem is that it will package EVERYTHING and it is not configurable. Should take CodeUri, but not in pure cloudformation. 
No CodeUri in ::Fucntion when not in Serveless, so ... stick to manual solution
aws cloudformation package --template-file template.yaml --s3-bucket lagartixa-lambda-functions --output-template-file packaged-template.yaml
```

###AWC CLI Lambda Deployment procedure - NO CloudFormation
```
zip function.zip handler.js
/c/Program\ Files/7-Zip/7z.exe a lambda.zip handler.js

aws iam create-role --role-name basic_lambda_role --assume-role-policy-document file://basic_lambda_role.json

aws lambda create-function --function-name my-function \
 --zip-file fileb://lambda.zip --handler handler.handler --runtime nodejs10.x \
 --role arn:aws:iam::811079058775:role/basic_lambda_role_2
```

###Cloudformation commands list
https://docs.aws.amazon.com/cli/latest/reference/cloudformation/index.html
```
aws cloudformation validate-template --template-body file://template.yaml
aws cloudformation create-stack --stack-name stk1 --template-body file://template.yaml --capabilities CAPABILITY_AUTO_EXPAND
aws cloudformation delete-stack --stack-name stk1
aws cloudformation wait stack-delete-complete --stack-name stk1
aws cloudformation describe-stacks --stack-name stk1 \
    --query 'Stacks[0].Outputs[?OutputKey==`InputEndpoint`].OutputValue' \
    --output text
```


**Redeployment** of APIs is not a *provisioning* task... It is a promotion activity which is part of a stage in your software release process.
So redeployment of this template will **not** make a new deployment of API Gateway! :(
Solution is to f.ex. change anything in the name or even **description** of deployment (time based name?).
OR Timestamp from variable:
```
ApiDeployment$TIMESTAMP$: # <- but there is a separate script inserrting this value
    Type: AWS::ApiGateway::Deployment
```
###Bibliography
https://currentlyunnamed-theclassic.blogspot.com/2018/12/mastering-cloudformation-for-api.html
https://nickolaskraus.org/articles/creating-an-amazon-api-gateway-with-a-mock-integration-using-cloudformation/


###Sequence diagram
![Alt text](documentation/SEQUENCE.png "Sequence Diagram")