'use strict';
const gulp = require('gulp');
const zip = require('gulp-zip');
const chmod = require('gulp-chmod');

exports.default = () => (
	gulp.src('src/**')
	    .pipe(chmod(0o755, true))
		.pipe(zip('nodelambda.zip'))
		.pipe(gulp.dest('target'))
);