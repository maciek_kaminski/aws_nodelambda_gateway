#!/bin/bash

if [ -z "$1" ]
  then
    echo "No Input Type supplied."
    echo "Possible arguments:"
    echo " ok - a 200 response with a JSON body"
    echo " fail - a 5xx logic response with a JSON body"
    echo " noService - a 4xx response with a small string message"
    exit 1
fi

case "$1" in
  "ok") ;;
  "fail") ;;
  "noService") ;;
  *)
    echo "Invalid argument supplied."
    echo "Possible arguments:"
    echo " ok - a 200 response with a JSON body"
    echo " fail - a 5xx logic response with a JSON body"
    echo " noService - a 4xx response with a small string message"
  exit 1
esac

inputType=$1

url=$(aws cloudformation describe-stacks --stack-name stk1 \
    --query 'Stacks[0].Outputs[?OutputKey==`InputEndpoint`].OutputValue' \
    --output text)
echo "Edge Api Gatewa URL: "
echo ${url}
echo ""
curl -X POST --silent --data "@documentation/${inputType}.json" -s -w '\nStatus code: %{http_code}\n' --header "Content-Type:application/json" ${url}/execLambda
